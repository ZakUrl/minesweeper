from kivy.uix.label import Label
from kivy.clock import Clock

class Timer(Label):
    def __init__(self, **kwargs):
        super(Timer,self).__init__(**kwargs)
        self.size_hint_x = 0.5
        self.size_hint_y = 0.2
        self.counter = 0
        self.event = None

    def update(self, *args):
        self.counter += 1
        self.text = "Time(in s) : " + str(self.counter)

    def launch(self):
        self.event = Clock.schedule_interval(self.update,1)

    def getTime(self):
        return self.counter
        
    def stop(self):
    	self.event.cancel()
