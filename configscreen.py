from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.checkbox import CheckBox
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from gamescreen import GameScreen
from timer import Timer


class ConfigScreen(Screen):

    def __init__(self,sm,**kwargs):
        super(ConfigScreen,self).__init__(**kwargs)
        self.sm = sm
        self.box = BoxLayout(orientation="vertical")
        self.titre = Label(text="choisir le niveau :")
        self.nom_joueur = Label(text="Enter your name : ")
        self.entree_joueur = TextInput(multiline=False)
        self.entree_joueur.bind(on_text_validate=self.get_input)
        self.jouer_bouton = Button(text="Play",on_press=self.play)
        self.create_levels()
        self.box.add_widget(self.titre)
        self.box.add_widget(self.nom_joueur)
        self.box.add_widget(self.entree_joueur)
        self.box.add_widget(self.main_layout)
        self.box.add_widget(self.jouer_bouton)
        self.add_widget(self.box)
        self.active_box = None
        self.player_name = ""
    
    def create_levels(self):
        self.easy_box = CheckBox()
        self.easy_box.bind(active=self.on_checkbox_active)
        self.medium_box = CheckBox()
        self.medium_box.bind(active=self.on_checkbox_active)
        self.hard_box = CheckBox()
        self.hard_box.bind(active=self.on_checkbox_active)
        #level easy = index 0 , medium = index 1, hard = index 2
        self.check_list = [self.easy_box,self.medium_box,self.hard_box]
        self.easy_lab = Label(text="Easy")
        self.medium_lab = Label(text="Medium")
        self.hard_lab = Label(text="Hard")
        self.main_layout = GridLayout(cols=2,rows=3)
        self.main_layout.add_widget(self.easy_lab)
        self.main_layout.add_widget(self.easy_box)
        self.main_layout.add_widget(self.medium_lab)
        self.main_layout.add_widget(self.medium_box)
        self.main_layout.add_widget(self.hard_lab)
        self.main_layout.add_widget(self.hard_box)

    def create_game(self):
        level = self.get_level()
        player_name = self.get_player_name()
        game_screen = GameScreen(self.sm,level,player_name,name="Game")
        return game_screen
    
    def play(self,instance):
        gm = self.create_game()
        gm.start_timer()
        self.sm.add_widget(gm)
        self.manager.current = 'Game'
    
    def on_checkbox_active(self,checkbox, value):
        #avoid having multiple boxes checked
        for ch in self.check_list:
            if ch != checkbox:
                ch.active = False
        #current box checked
        self.active_box = checkbox

    def get_level(self):
        #return the index of the active checkbox
        if self.active_box:
            return self.check_list.index(self.active_box)

    def get_player_name(self):
        return self.player_name

    def get_input(self,instance):
        self.player_name = instance.text




