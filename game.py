from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from random import randint

#Global variables
LEVELS = {0:{'height':20,'width':20,'bomb':20},
          1:{'height':20,'width':25,'bomb':50},
          2:{'height':20,'width':30,'bomb':120}}
LOST = "No Luck..."
WIN = "You're the winner !"


class MineSweeper(GridLayout):

    
    def __init__(self,gameSc,level,**kwargs):
        super(MineSweeper,self).__init__(**kwargs)
        self.gameSc = gameSc
        self.rows = LEVELS[level]['height']
        self.cols = LEVELS[level]['width']
        self.bombNbr = LEVELS[level]['bomb']
        self.listBomb = []
        self.field = []
        self.createField()
        self.createBomb()

    def createField(self):
        #create all buttons
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                cell = Button()
                cell.bind(on_press=self.action)
                #add to layout
                self.add_widget(cell)
                row.append(cell)
            self.field.append(row)

    def createBomb(self):
        #Fill up the field with bombs
        bombCounter = 0
        while bombCounter < self.bombNbr:
            y = randint(0,self.rows-1)
            x = randint(0,self.cols-1)
            bomb = (y,x)
            if not(bomb in self.listBomb):
                self.listBomb.append(bomb)
                bombCounter += 1

    def findCell(self,button):
        x,y = 0,0
        found = False
        i = 0
        while i < self.rows and not(found):
            j = 0
            while j < self.cols and not(found):
                if self.field[i][j] == button:
                    x,y = j,i
                    found = True
                j += 1
            i += 1
        return x,y

    def action(self,instance):
        x,y = self.findCell(instance)
        self.reveal(x,y)

    def bombCounter(self,x,y):
        bombcounter = 0
        for j in range(-1,2):
            for i in range(-1,2):
                if 0 <= y+j < self.rows and 0 <= x+i < self.cols:
                    cell = (y+j,x+i)
                    if cell in self.listBomb:
                        bombcounter += 1
        return bombcounter

    def getDiscoveredCellNbr(self):
        counter = 0
        for row in self.field:
            for button in row:
                if button.disabled == True:
                    counter += 1
        return counter

    def getScore(self):
        return 100*((self.getDiscoveredCellNbr())/((self.rows*self.cols)-self.bombNbr))

    def explore(self,x,y):
        #if the position is valid
        if 0 <= y < self.rows and 0 <= x < self.cols:
            if not(self.field[y][x].disabled):
                bombNbr = self.bombCounter(x,y)
                if not(bombNbr):
                    self.field[y][x].disabled = True
                    for j in range(-1,2):
                        for i in range(-1,2):
                            if 0 <= y+j < self.rows and 0 <= x+i < self.cols:
                                self.explore(x+i,y+j)
                else:
                    self.field[y][x].text = str(bombNbr)
                    self.field[y][x].disabled = True

    def checkVictory(self):
        undiscovered = 0
        for i in range(len(self.field)):
            for j in range(len(self.field[i])):
                if not(self.field[i][j].disabled):
                    undiscovered += 1
        return undiscovered == self.bombNbr

    def reveal(self,x,y):
        #check if steps on a bomb
        if (y,x) in self.listBomb:
            self.field[y][x].background_color = [127, 51, 58, 1] 
            self.gameSc.end(LOST)
        else:
            self.explore(x,y)
            #check if victory
            if self.checkVictory():
                self.gameSc.end(WIN_MSG)