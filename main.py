import kivy
kivy.require('1.10.1') 
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.boxlayout import BoxLayout
from menu import MenuSc
from configuration import ConfigScreen



class Handler:
	"""Gestionnaire de fenetres"""

	def __init__(self):
		self.scManager = ScreenManager()
		self.menu = MenuSc(self.scManager,name='menu')
		self.conf = ConfigScreen(self.scManager,name='conf')
		self.scManager.add_widget(self.menu)
		self.scManager.add_widget(self.conf)

	def getScManager(self):
		return self.scManager


class GameApp(App):

	def build(self):
		handler = Handler()
		return handler.getScManager()

def main():
	GameApp().run()


if __name__ == '__main__':
	main()
