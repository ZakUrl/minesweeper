from kivy.app import App
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from score import loadScore
from scoredisplay import ScoreSc

class MenuSc(Screen):
    
    def __init__(self,scManager,**kwargs):
        super(MenuSc,self).__init__(**kwargs)
        self.scManager = scManager
        self.create_layout()

    def create_layout(self):
        self.layout = BoxLayout(orientation='horizontal')
        self.playBut = Button(text="Play",on_press=self.play)
        self.bestScoreBut = Button(text="Best score",on_press=self.showScore)
        self.quitBut = Button(text="Quit",on_press=self.quit)
        self.layout.add_widget(self.playBut)
        self.layout.add_widget(self.bestScoreBut)
        self.layout.add_widget(self.quitBut)
        self.add_widget(self.layout)

    def play(self,instance):
        self.manager.current = 'conf'

    def showScore(self,instance):
        best = loadScore()
        sc = ScoreSc(self.scManager,best,name="score")
        self.scManager.add_widget(sc)
        self.manager.current = "score"

    def quit(self,instance):
        if App.get_running_app():
            App.get_running_app().stop()