from score import Score
from timer import Timer
from game import MineSweeper
from endgame import EndSc
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label



class GameSc(Screen):
    def __init__(self,sm,level,playerName,**kwargs):
        super(GameSc,self).__init__(**kwargs)
        self.sm = sm
        self.level = level
        self.playerName = playerName
        self.createLayout()
        self.startTimer()

    def createLayout(self):
        self.playerLabel = Label(text="Player : " + self.playerName,size_hint_y = 0.2)
        self.timer = Timer()
        self.minesweeper = MineSweeper(self,self.level)
        self.main_layout = BoxLayout(orientation="vertical")
        self.main_layout.add_widget(self.playerLabel)
        self.main_layout.add_widget(self.timer)
        self.main_layout.add_widget(self.minesweeper)
        self.add_widget(self.main_layout)


    def startTimer(self):
        self.timer.launch()

    def end(self,msg):
        self.timer.stop()
        afg = EndSc(self.sm,msg,name="after")
        self.sm.add_widget(afg)
        self.manager.current = "after"

    def getScore(self):
        s = self.minesweeper.getScore()
        score = Score(s,self.playerName,self.timer.getTime(),self.level)
        return score

