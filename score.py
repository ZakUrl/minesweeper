from json import dump, load

#global vars
JSON_FILE = "scores.json"
LEVELS = {0:"Easy",1:"Medium",2:"Hard"}


class Score:
	
	def __init__(self,score,name,time,level):
		self.score = score
		self.name = name
		self.time = time
		self.level = LEVELS[level]
		self.score_entry = {"score":self.score,"name":self.name,
		                    "time":self.time,"level":self.level}
	def get_score(self):
		return self.score_entry 

def loadScore():
	scores = []
	try:
		with open(JSON_FILE,"r",encoding="utf-8") as json_file:
			for line in json_file:
				scores.append(eval(line))
		best = sorted(scores,key=lambda elem: elem["score"],reverse=True)[0]
	except FileNotFoundError:
		best = None
	return best
		

def saveScore(score):
	with open(JSON_FILE,"a",encoding="utf-8") as json_file:
		dump(score.get_score(),json_file,ensure_ascii=False)
		json_file.write("\n")






