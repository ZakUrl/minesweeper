from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.button import Button

class ScoreSc(Screen):

	def __init__(self,sm,score,**kwargs):
		super(ScoreSc,self).__init__(**kwargs)
		self.sm = sm
		self.createLayout(score)


	def createLayout(self,score):
		self.main_layout = BoxLayout(orientation="vertical")
		self.returnBut = Button(text="Return to menu",on_press=self.back)
		#if a score exists
		if score :
			self.score = score
			self.score_lab = Label(text="Score : "+str(self.score["score"]))
			self.player_lab = Label(text="Player : "+self.score["name"])
			self.time_lab = Label(text="Time : "+str(self.score["time"])+" s")
			self.level_lab = Label(text="Level : "+self.score["level"])
			self.main_layout.add_widget(self.score_lab)
			self.main_layout.add_widget(self.time_lab)
			self.main_layout.add_widget(self.level_lab)
			self.main_layout.add_widget(self.player_lab)
		else:
			self.lab = Label(text="No score saved")
			self.main_layout.add_widget(self.lab)
		self.main_layout.add_widget(self.returnBut)
		self.add_widget(self.main_layout)

	def back(self,instance):
		self.manager.current = "menu"
		self.sm.remove_widget(self)


