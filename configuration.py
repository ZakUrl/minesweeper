from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.checkbox import CheckBox
from kivy.uix.textinput import TextInput
from gamedisplay import GameSc
from timer import Timer


class ConfigScreen(Screen):

    def __init__(self,sm,**kwargs):
        super(ConfigScreen,self).__init__(**kwargs)
        self.sm = sm #screen manager
        self.createLayout()
        self.namePlayer = ""
        self.chkBox = None

    def createLayout(self):
        self.box = BoxLayout(orientation="vertical")
        self.levelLab = Label(text="Choose your level")
        self.nameLab = Label(text="Enter your name(press enter to validate) : ")
        self.nameInp = TextInput(multiline=False)
        self.nameInp.bind(on_text_validate=self.getNameInp)
        self.playBut = Button(text="Play",on_press=self.play)
        self.create_levels()
        self.box.add_widget(self.nameLab)
        self.box.add_widget(self.nameInp)
        self.box.add_widget(self.levelLab)
        self.box.add_widget(self.main_layout)
        self.box.add_widget(self.playBut)
        self.add_widget(self.box)
    
    def create_levels(self):
        self.easy_box = CheckBox()
        self.easy_box.bind(active=self.on_checkbox_active)
        self.medium_box = CheckBox()
        self.medium_box.bind(active=self.on_checkbox_active)
        self.hard_box = CheckBox()
        self.hard_box.bind(active=self.on_checkbox_active)
        #level easy = index 0 , medium = index 1, hard = index 2
        self.check_list = [self.easy_box,self.medium_box,self.hard_box]
        self.easy_lab = Label(text="Easy")
        self.medium_lab = Label(text="Medium")
        self.hard_lab = Label(text="Hard")
        self.main_layout = GridLayout(cols=2,rows=3)
        self.main_layout.add_widget(self.easy_lab)
        self.main_layout.add_widget(self.easy_box)
        self.main_layout.add_widget(self.medium_lab)
        self.main_layout.add_widget(self.medium_box)
        self.main_layout.add_widget(self.hard_lab)
        self.main_layout.add_widget(self.hard_box)

    def create_game(self):
        level = self.getLevel()
        player_name = self.getPlayerName()
        game_screen = GameSc(self.sm,level,player_name,name="game")
        return game_screen

    def getPlayerName(self):
        return self.namePlayer

    def getNameInp(self,instance):
        self.namePlayer = instance.text
    
    def play(self,instance):
        gm = self.create_game()
        self.sm.add_widget(gm)
        self.manager.current = 'game'

    
    def on_checkbox_active(self,checkbox, value):
        for ch in self.check_list:
            if ch != checkbox:
                ch.active = False
        #current box checked
        self.chkBox = checkbox

    def getLevel(self):
        #return the index of the active checkbox
        if self.chkBox:
            level = self.check_list.index(self.chkBox)
            return level






