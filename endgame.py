from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from score import saveScore

class EndSc(Screen):

    def __init__(self,sm,msg,**kwargs):
        super(EndSc,self).__init__(**kwargs)
        self.sm = sm
        self.msg = msg
        self.prev_screen = self.sm.get_screen("game")
        self.score = self.prev_screen.getScore()
        self.createLayout()
        

    def createLayout(self):
        self.main_layout = BoxLayout(orientation="vertical")
        self.msgLab = Label(text=self.msg)
        self.save_but = Button(text="Save",on_press=self.save)
        self.returnMenu = Button(text="Return To Menu",on_press=self.return_to)
        self.main_layout.add_widget(self.msgLab)
        self.main_layout.add_widget(self.save_but)
        self.main_layout.add_widget(self.returnMenu)
        self.add_widget(self.main_layout)


    def return_to(self,instance):
        self.sm.remove_widget(self.prev_screen)
        self.manager.current = "menu"
        self.sm.remove_widget(self)

    def save(self,instance):
        saveScore(self.score)
        
